**Author**: MA Junwei

**Purpose**: For MYOB code exercise

**How to compile the application?**: mvn clean package

**How to run the application?**: After built, run the executable jar file with one argument of CSV file absolute path. e.g.: java -jar *<archive>*.jar *<absolute path of csv file>*, a sample csv input is under src/main/resources

**Input CSV format supported**: The input csv file should be comma seperated, headers should be in sequence of: first name, last name, annual salary, super rate (%), payment start date

**Output CSV format**: ["name", "pay period", "gross income", "income tax", "net income", "super"]

**Test coverage**: The application is developed following TDD, and most of class + functions are covered with unit test. For those simple functions or 3rd parth library functions, no test is covered.
