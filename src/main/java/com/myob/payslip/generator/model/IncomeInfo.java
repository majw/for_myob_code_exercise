package com.myob.payslip.generator.model;

import java.io.Serializable;

import lombok.Data;

/**
 * a model to present the information of a person's income
 * 
 * @author Junwei
 *
 */

@Data
public class IncomeInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// first name of employee
	private String firstName;

	// last name of employee
	private String lastName;

	// annual salary of employee
	private int annualSalary;

	// super rate
	private double superRate;

	// payment period
	private String paymentPeriod;
}
