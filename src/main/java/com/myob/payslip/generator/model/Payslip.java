package com.myob.payslip.generator.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.stream.Collectors;

import org.springframework.util.Assert;

import lombok.Data;

/**
 * a model to present the output of a payslip
 * 
 * @author Junwei
 *
 */
@Data
public class Payslip implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// name of employee
	private String name;

	// period of current payslip
	private String payPeriod;

	// gross income during the period
	private int grossIncome;

	// income tax during the period
	private int incomeTax;

	// net income during the period
	private int netIncome;

	// super during the peried
	private int superAmount;

	public void setName(String... names) {
		Assert.notEmpty(names, "names cannot be null or empty");
		this.name = Arrays.asList(names).stream().collect(Collectors.joining(Constants.WHITESPACE));
	}

}
