package com.myob.payslip.generator.model;

/**
 * to define constant value in the whole application
 * 
 * @author Junwei
 *
 */
public interface Constants {

	static final String WHITESPACE = " ";

	static final String COMMA = ",";

	static final String DASH = "-";
}
