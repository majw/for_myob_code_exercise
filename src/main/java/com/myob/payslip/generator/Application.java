package com.myob.payslip.generator;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import com.myob.payslip.generator.executor.MonthlyPayslipGenerator;
import com.myob.payslip.generator.model.IncomeInfo;
import com.myob.payslip.generator.model.Payslip;
import com.myob.payslip.generator.util.CsvUtil;

public class Application {

	public static void main(String[] args) {
		if (Objects.isNull(args) || args.length != 1) {
			System.err.println("this generator takes one argument for csv file path");
			System.exit(1);
		}

		try {
			// read the csv file to a collection of income info.
			List<IncomeInfo> incomeInfos = CsvUtil.read(args[0], true);

			// generate a collection of payslip information based on the income
			// info.
			Collection<Payslip> payslips = MonthlyPayslipGenerator.generate(incomeInfos);
			CsvUtil.write(payslips);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
