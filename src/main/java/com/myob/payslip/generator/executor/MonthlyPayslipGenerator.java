package com.myob.payslip.generator.executor;

import java.util.Collection;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.util.Assert;

import com.myob.payslip.generator.model.IncomeInfo;
import com.myob.payslip.generator.model.Payslip;

/**
 * this generator is to generate payslip information from given arguments.
 * 
 * @author Junwei
 *
 */
public class MonthlyPayslipGenerator {

	/**
	 * generate a collection of payslip from given income info collection.
	 * 
	 * @param incomeInfos
	 * @return
	 */
	public static Collection<Payslip> generate(Collection<IncomeInfo> incomeInfos) {
		Assert.notEmpty(incomeInfos, "income infos to generate payslip cannot be null or empty");

		return incomeInfos.stream().map(translator).collect(Collectors.toList());
	}

	/**
	 * this translator function is to generate a payslip from input income info
	 * model.
	 */
	private static Function<IncomeInfo, Payslip> translator = incomeInfo -> {
		Payslip payslip = new Payslip();
		payslip.setName(incomeInfo.getFirstName(), incomeInfo.getLastName());
		payslip.setPayPeriod(incomeInfo.getPaymentPeriod());

		payslip.setGrossIncome(MonthlyAmountCalculator.findGrossIncome(incomeInfo.getAnnualSalary()));
		payslip.setIncomeTax(MonthlyAmountCalculator.findIncomeTax(incomeInfo.getAnnualSalary()));

		payslip.setNetIncome(MonthlyAmountCalculator.findNetIncome(payslip.getGrossIncome(), payslip.getIncomeTax()));
		payslip.setSuperAmount(
				MonthlyAmountCalculator.findSuperAmount(payslip.getGrossIncome(), incomeInfo.getSuperRate()));

		return payslip;
	};
}
