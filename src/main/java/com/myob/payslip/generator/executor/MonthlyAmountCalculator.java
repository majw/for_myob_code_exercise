package com.myob.payslip.generator.executor;

import org.springframework.util.Assert;

import com.myob.payslip.generator.strategy.TaxStrategy;
import com.myob.payslip.generator.strategy.TaxStrategyFactory;

/**
 * this calculator is to calculate amounts in monthly payslip
 * 
 * @author Junwei
 *
 */
public class MonthlyAmountCalculator {

	/**
	 * select a proper tax strategy for incoming amount.
	 * 
	 * @param amount
	 * @return
	 */
	public static int findIncomeTax(int amount) {
		for (TaxStrategy strategy : TaxStrategyFactory.STRATEGIES) {
			if (strategy.withinRange(amount)) {
				return strategy.findMonthlyIncomeTax(amount);
			}
		}
		throw new RuntimeException(String.format("income amount: %s is not in any range of tax strategy.", amount));
	}

	public static int findGrossIncome(int amount) {

		Assert.isTrue(amount >= 0, String.format("amount %s cannot be negative", amount));

		return (int) Math.round(amount / 12D);
	}

	public static int findNetIncome(int grossIncome, int incomeTax) {
		Assert.isTrue(grossIncome >= 0, String.format("gross income %s cannot be negative", grossIncome));

		Assert.isTrue(incomeTax >= 0, String.format("income tax %s cannot be negative", incomeTax));

		Assert.isTrue(grossIncome >= incomeTax,
				String.format("gross income %s cannot be less than income tax %s", grossIncome, incomeTax));
		return grossIncome - incomeTax;
	}

	public static int findSuperAmount(int grossIncome, double superRate) {

		Assert.isTrue(grossIncome >= 0, String.format("gross income %s cannot be negative", grossIncome));

		Assert.isTrue(superRate >= 0, String.format("super rate %s cannot be negative", superRate));
		return (int) Math.round(grossIncome * superRate);
	}
}
