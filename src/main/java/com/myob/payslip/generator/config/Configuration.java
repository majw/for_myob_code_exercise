package com.myob.payslip.generator.config;

/**
 * configuration of this application.
 * 
 * @author Junwei
 *
 */
public interface Configuration {

	interface CSV {
		// the index of first name in the csv file
		static final int FIRST_NAME_IDX = 0;

		// the index of last name in the csv file
		static final int LAST_NAME_IDX = 1;

		// the index of annual salary in the csv file
		static final int ANNUAL_SALARY_IDX = 2;

		// the index of super rate in the csv file
		static final int SUPER_RATE_IDX = 3;

		// the index of payment start date in the csv file
		static final int PAYMENT_START_DATE_IDX = 4;

		// headers in the output csv file
		static final String[] OUTPUT_HEADERS = new String[] { "name", "pay period", "gross income", "income tax",
				"net income", "super" };
	}

	interface TaxTable {
		static final int LVL0_MIN = 0;
		static final int LVL0_MAX = 18200;
		static final int LVL0_BASE = 0;
		static final double LVL0_TAX_RATE = 0;

		static final int LVL1_MIN = 18201;
		static final int LVL1_MAX = 37000;
		static final int LVL1_BASE = 0;
		static final double LVL1_TAX_RATE = 0.19D;

		static final int LVL2_MIN = 37001;
		static final int LVL2_MAX = 80000;
		static final int LVL2_BASE = 3572;
		static final double LVL2_TAX_RATE = 0.325D;

		static final int LVL3_MIN = 80001;
		static final int LVL3_MAX = 180000;
		static final int LVL3_BASE = 17547;
		static final double LVL3_TAX_RATE = 0.37D;

		static final int LVL4_MIN = 180001;
		static final int LVL4_MAX = Integer.MAX_VALUE;
		static final int LVL4_BASE = 54547;
		static final double LVL4_TAX_RATE = 0.45D;
	}

}
