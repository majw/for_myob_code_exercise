package com.myob.payslip.generator.util;

import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.myob.payslip.generator.config.Configuration;
import com.myob.payslip.generator.model.IncomeInfo;
import com.myob.payslip.generator.model.Payslip;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

/**
 * a util to read a csv file to income info model and output payslip model to
 * csv
 * 
 * <p>
 * the input/output csv headers are defined in configuration. if any format
 * change in the csv, the configurations should be changed accordingly.
 * </p>
 * 
 * @author Junwei
 *
 */
public class CsvUtil {

	/**
	 * read a csv file and get income info models
	 * 
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	public static List<IncomeInfo> read(String fileName, boolean hasHeader) throws IOException {
		CSVReader reader = null;
		try {
			List<IncomeInfo> incomeInfos = new ArrayList<>();
			reader = new CSVReader(new FileReader(fileName));
			if (hasHeader) {
				reader.readNext(); // skip the headers if file has header.
			}
			String[] line;
			while ((line = reader.readNext()) != null) {
				IncomeInfo info = new IncomeInfo();
				info.setFirstName(line[Configuration.CSV.FIRST_NAME_IDX]);
				info.setLastName(line[Configuration.CSV.LAST_NAME_IDX]);
				info.setAnnualSalary(Integer.valueOf(line[Configuration.CSV.ANNUAL_SALARY_IDX]));
				info.setSuperRate(Double.valueOf(line[Configuration.CSV.SUPER_RATE_IDX].split("%")[0]) / 100);
				info.setPaymentPeriod(line[Configuration.CSV.PAYMENT_START_DATE_IDX]);

				incomeInfos.add(info);
			}
			return incomeInfos;
		} finally {
			reader.close();
		}
	}

	/**
	 * write payslips into a csv file.
	 * 
	 * @param payslips
	 */
	public static void write(Collection<Payslip> payslips) {
		StringWriter writer = new StringWriter();

		CSVWriter csvWriter = new CSVWriter(writer);

		List<String[]> data = toStringArray(payslips);

		csvWriter.writeAll(data);

		try {
			csvWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println(writer);
	}

	private static List<String[]> toStringArray(Collection<Payslip> payslips) {
		List<String[]> records = new ArrayList<String[]>();

		// adding header record
		records.add(Configuration.CSV.OUTPUT_HEADERS);

		Iterator<Payslip> it = payslips.iterator();
		while (it.hasNext()) {
			Payslip payslip = it.next();
			records.add(new String[] { payslip.getName(), payslip.getPayPeriod(),
					String.valueOf(payslip.getGrossIncome()), String.valueOf(payslip.getIncomeTax()),
					String.valueOf(payslip.getNetIncome()), String.valueOf(payslip.getSuperAmount()) });
		}
		return records;
	}
}
