package com.myob.payslip.generator.strategy;

import org.springframework.util.Assert;

/**
 * this class is to define a tax strategy based on the given tax table.
 * <p>
 * each row in tax table will have one tax strategy instance.
 * </p>
 * 
 * @author Junwei
 *
 */
public class TaxStrategy {

	// base tax of strategy
	private final int base;

	// tax rate of strategy
	private final double taxRate;

	// min amount of income of strategy
	private final int min;

	// max amount of income of strategy
	private final int max;

	public TaxStrategy(int base, double taxRate, int min, int max) {
		Assert.isTrue(base >= 0, "base of tax strategy cannot be negative");
		Assert.isTrue(taxRate >= 0, "tax rate of tax strategy cannot be negative");
		Assert.isTrue(min >= 0, "min amount of tax strategy cannot be negative");
		Assert.isTrue(max >= 0, "max amount of tax strategy cannot be negative");
		Assert.isTrue(max >= min, "max amount of tax strategy must greater or equal to min amount of tax strategy");
		this.base = base;
		this.taxRate = taxRate;
		this.min = min;
		this.max = max;
	}

	/**
	 * get income tax per month for this strategy instance.
	 * 
	 * @param amount
	 * @return
	 */
	public int findMonthlyIncomeTax(int amount) {
		Assert.isTrue(amount >= 0, String.format("the income amount %s cannot be negative", amount));
		Assert.isTrue(min <= amount && max >= amount,
				String.format("the income amount is beyond the strategy range %s - %s", min, max));

		return (int) Math.round((base + (amount - min + 1) * taxRate) / 12);
	}

	/**
	 * checking the given amount is falling in this strategy or not.
	 * 
	 * @param amount
	 * @return
	 */
	public boolean withinRange(int amount) {
		Assert.isTrue(amount >= 0, String.format("the income amount %s cannot be negative", amount));
		return amount >= min && amount <= max;
	}

}
