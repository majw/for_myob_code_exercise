package com.myob.payslip.generator.strategy;

import java.util.Arrays;
import java.util.List;

import com.myob.payslip.generator.config.Configuration;

/**
 * a factory to hold all tax strategies.
 * <p>
 * tax configuration are from configuration file.
 * </p>
 * 
 * @author Junwei
 *
 */
public class TaxStrategyFactory {

	public final static List<TaxStrategy> STRATEGIES = Arrays.asList(lvl0TaxStrategy(), lvl1TaxStrategy(),
			lvl2TaxStrategy(), lvl3TaxStrategy(), lvl4TaxStrategy());

	// tax strategy 0 in the taxable income table - Nil
	private static TaxStrategy lvl0TaxStrategy() {
		return new TaxStrategy(Configuration.TaxTable.LVL0_BASE, Configuration.TaxTable.LVL0_TAX_RATE,
				Configuration.TaxTable.LVL0_MIN, Configuration.TaxTable.LVL0_MAX);
	}

	// tax strategy 1 in the taxable income table - (18201, 37000)
	private static TaxStrategy lvl1TaxStrategy() {
		return new TaxStrategy(Configuration.TaxTable.LVL1_BASE, Configuration.TaxTable.LVL1_TAX_RATE,
				Configuration.TaxTable.LVL1_MIN, Configuration.TaxTable.LVL1_MAX);
	}

	// tax strategy 2 in the taxable income table - (37001, 80000)
	private static TaxStrategy lvl2TaxStrategy() {
		return new TaxStrategy(Configuration.TaxTable.LVL2_BASE, Configuration.TaxTable.LVL2_TAX_RATE,
				Configuration.TaxTable.LVL2_MIN, Configuration.TaxTable.LVL2_MAX);
	}

	// tax strategy 3 in the taxable income table - (80001, 180000)
	private static TaxStrategy lvl3TaxStrategy() {
		return new TaxStrategy(Configuration.TaxTable.LVL3_BASE, Configuration.TaxTable.LVL3_TAX_RATE,
				Configuration.TaxTable.LVL3_MIN, Configuration.TaxTable.LVL3_MAX);
	}

	// tax strategy 4 in the taxable income table - (180001,)
	private static TaxStrategy lvl4TaxStrategy() {
		return new TaxStrategy(Configuration.TaxTable.LVL4_BASE, Configuration.TaxTable.LVL4_TAX_RATE,
				Configuration.TaxTable.LVL4_MIN, Configuration.TaxTable.LVL4_MAX);
	}

}
