Feature: unit test function generate in class TaxStrategy

Scenario Outline: 
	Given the tax strategy configuration is <base>, <taxRate>, <min>, <max>, and an amount <amount>
	When we test function findMonthlyIncomeTax
	Then the return of findMonthlyIncomeTax should be <result>

	Examples:
		|    base    | taxRate    |  min      | max        | amount | result |
        |    0 		 | 0 		  |  0        | 18200      | 0      |   0    |
        |    0 		 | 0 		  |  0        | 18200      | 1000   |   0    |
        |    0 		 | 0 		  |  0        | 18200      | 18200  |   0    |
	    |    0 		 | 0.19 	  |  18201    | 37000      | 18201  |   0    |
	    |    0 		 | 0.19 	  |  18201    | 37000      | 30000  |   187  |
	    |    0 		 | 0.19 	  |  18201    | 37000      | 37000  |   298  |
	    |    3572    | 0.325 	  |  37001    | 80000      | 37001  |   298  |
	    |    3572    | 0.325 	  |  37001    | 80000      | 50000  |   650  |
	    |    3572    | 0.325 	  |  37001    | 80000      | 80000  |   1462 |
	    |    17547 	 | 0.37 	  |  80001    | 180000     | 80001  |   1462 |
	    |    17547 	 | 0.37 	  |  80001    | 180000     | 100000 |   2079 |
	    |    17547 	 | 0.37 	  |  80001    | 180000     | 180000 |   4546 |
	    |    54547 	 | 0.45 	  |  180001   | 2147483647 | 180001 |   4546 |
	    |    54547 	 | 0.45 	  |  180001   | 2147483647 | 180001 |   4546 |
	    |    54547 	 | 0.45 	  |  180001   | 2147483647 | 300000 |   9046 |
	    
        |    0 		 | 0 		  |  0        | 18200      | -1000  |  FAILED|
        |    0 		 | 0 		  |  0        | 18200      | 18201  |  FAILED|