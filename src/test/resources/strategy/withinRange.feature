Feature: unit test function withinRange in class TaxStrategy

Scenario Outline: 
	Given the tax strategy configuration is <base>, <taxRate>, <min>, <max>, and an amount <amount>
	When we test function withinRange
	Then the return of withinRange should be <result>

	Examples:
		|    base    | taxRate    |  min | max   | amount | result |
	    |    0 		 | 0 		  |  0   | 18200 | -1     | FAILED |
	    |    0 		 | 0 		  |  0   | 18200 | 0      | true   |
	    |    0 		 | 0 		  |  0   | 18200 | 18199  | true   |
	    |    0 		 | 0 		  |  0   | 18200 | 18200  | true   |
	    |    0 		 | 0 		  |  0   | 18200 | 18201  | false  |