Feature: unit test constructor in class TaxStrategy

Scenario Outline: 
	Given the tax strategy configuration is <base>, <taxRate>, <min>, <max>
	When we test the constructor
	Then the execution of constructor should be <result>

	Examples:
		|    base    | taxRate    |  min      | max        | result |
	    |    0 		 | 0 		  |  0        | 18200      | true   |
	    |    0 		 | 0.19 	  |  18201    | 37000      | true   |
	    |    3572    | 0.325 	  |  37001    | 80000      | true   |
	    |    17547 	 | 0.37 	  |  80001    | 180000     | true   |
	    |    54547 	 | 0.45 	  |  180001   | 2147483647 | true   |
	    
	    |    -54547	 | 0.45 	  |  180001   | 2147483647 | false  |
	    |    54547 	 | -0.45 	  |  180001   | 2147483647 | false  |
	    |    54547 	 | 0.45 	  |  -180001  | 2147483647 | false  |
	    |    54547 	 | 0.45 	  |  180001   | -122222222 | false  |	    
	    |    54547 	 | 0.45 	  |  180001   | 0          | false  |