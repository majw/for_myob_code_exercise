Feature: test the gross income of an employee get per month based on his annual salary

Scenario Outline: 
	Given the annual income is <amount> for gross income testing
	When we get the employee's gross income per month
	Then the result should be <grossIncome> for gross income testing

	Examples:
		|    amount      | grossIncome      |
	    |    -1		     | FAILED		    |
	    |    0 		     | 0 			    |
	    |    18200 		 | 1517 			|
	    |    18201 		 | 1517 			|
	    |    18232 		 | 1519 			|
	    |    60050 		 | 5004 			|
	    |    120000 	 | 10000 		    |