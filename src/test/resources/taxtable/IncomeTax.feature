Feature: test the income tax an employee should pay per month based on his annual salary

Scenario Outline: 
	Given the annual income is <amount>
	When we get the employee's income tax per month
	Then the result should be <incomeTax>

	Examples:
		|    amount      | incomeTax    |
	    |    -100 		 | FAILED 		|
	    |    0 		     | 0 			|
	    |    18200 		 | 0 			|
	    |    18201 		 | 0 			|
	    |    18232 		 | 1 			|
	    |    60050 		 | 922 			|
	    |    120000 	 | 2696 		|