Feature: test the super of an employee get per month based on his gross income and super rate

Scenario Outline: 
	Given the gross income is <grossIncome> and super rate is <superRate>
	When we get the employee's super per month
	Then the super should be <super>

	Examples:
		|    grossIncome | superRate        | super     |
	    |    0 		     | -1 			    | FAILED    |
	    |    -1 		 | 0 			    | FAILED    |
	    |    -1		     | -1 			    | FAILED    |
	    |    0 		     | 0 			    | 0         |
	    |    2000 		 | 0 			    | 0         |
	    |    5004 		 | 0.09 			| 450       |
	    |    10000 		 | 0.1  			| 1000      |