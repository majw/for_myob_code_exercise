Feature: test the net income of an employee get per month based on his gross income and income tax

Scenario Outline: 
	Given the gross income is <grossIncome> and income tax is <incomeTax>
	When we get the employee's net income per month
	Then the net income should be <netIncome>

	Examples:
		|    grossIncome | incomeTax        | netIncome |
	    |    0 		     | -1 			    | FAILED    |
	    |    -1 		 | 0 			    | FAILED    |
	    |    -1		     | -1 			    | FAILED    |
	    |    0 		     | 0 			    | 0         |
	    |    2000 		 | 1000 			| 1000      |
	    |    3000 		 | 1000 			| 2000      |