package com.myob.taxtable;

import org.junit.Assert;

import com.myob.payslip.generator.executor.MonthlyAmountCalculator;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class IncomeTaxTestStep {

	int amount;

	int incomeTax;

	boolean hasException;

	@Given("^the annual income is (-?\\d+)$")
	public void the_annual_income_is(int arg1) throws Throwable {
		this.amount = arg1;
	}

	@When("^we get the employee's income tax per month$")
	public void we_get_the_employee_s_income_tax_per_month() throws Throwable {
		try {
			incomeTax = MonthlyAmountCalculator.findIncomeTax(amount);

		} catch (IllegalArgumentException ex) {
			this.hasException = true;
		}
	}

	@Then("^the result should be (\\d+)$")
	public void the_result_should_be(int arg1) throws Throwable {
		Assert.assertEquals(arg1, incomeTax);
	}

	@Then("^the result should be FAILED$")
	public void the_result_should_be_FAILED() throws Throwable {
		Assert.assertTrue(hasException);
	}

}
