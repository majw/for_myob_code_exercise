package com.myob.taxtable;

import org.junit.Assert;

import com.myob.payslip.generator.executor.MonthlyAmountCalculator;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SuperTestStep {
	int grossIncome;

	double superRate;

	int superAmount;

	boolean hasException;

	@Given("^the gross income is (-?\\d+) and super rate is (-?\\d*\\.?\\d+)$")
	public void the_gross_income_is_and_super_rate_is(int arg1, double arg2) throws Throwable {
		this.grossIncome = arg1;
		superRate = arg2;
	}

	@When("^we get the employee's super per month$")
	public void we_get_the_employee_s_super_per_month() throws Throwable {
		try {
			superAmount = MonthlyAmountCalculator.findSuperAmount(grossIncome, superRate);
		} catch (IllegalArgumentException e) {
			this.hasException = true;
		}
	}

	@Then("^the super should be (\\d+)$")
	public void the_super_should_be(int arg1) throws Throwable {
		Assert.assertEquals(arg1, superAmount);
	}

	@Then("^the super should be FAILED$")
	public void the_super_should_be_FAILED() throws Throwable {
		Assert.assertTrue(hasException);
	}
}
