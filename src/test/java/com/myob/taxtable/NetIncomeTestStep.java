package com.myob.taxtable;

import org.junit.Assert;

import com.myob.payslip.generator.executor.MonthlyAmountCalculator;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class NetIncomeTestStep {
	int grossIncome;

	int incomeTax;

	int netIncome;

	boolean hasException;

	@Given("^the gross income is (-?\\d+) and income tax is (-?\\d+)$")
	public void the_gross_income_is_and_income_tax_is(int arg1, int arg2) throws Throwable {
		this.grossIncome = arg1;
		this.incomeTax = arg2;
	}

	@When("^we get the employee's net income per month$")
	public void we_get_the_employee_s_net_income_per_month() throws Throwable {
		try {
			this.netIncome = MonthlyAmountCalculator.findNetIncome(grossIncome, incomeTax);
		} catch (IllegalArgumentException e) {
			this.hasException = true;
		}
	}

	@Then("^the net income should be (\\d+)$")
	public void the_net_income_should_be(int arg1) throws Throwable {
		Assert.assertEquals(arg1, netIncome);
	}

	@Then("^the net income should be FAILED$")
	public void the_net_income_should_be_FAILED() throws Throwable {
		Assert.assertTrue(hasException);
	}
}
