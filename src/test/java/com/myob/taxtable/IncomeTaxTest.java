package com.myob.taxtable;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = { "src/test/resources/taxtable/IncomeTax.feature" } // refer
																		// to
// Feature
// file
)
public class IncomeTaxTest {
}
