package com.myob.taxtable;

import org.junit.Assert;

import com.myob.payslip.generator.executor.MonthlyAmountCalculator;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GrossIncomeTestStep {
	int amount;

	int grossIncome;

	boolean hasException = false;

	@Given("^the annual income is (-?\\d+) for gross income testing$")
	public void the_annual_income_is_for_gross_income_testing(int arg1) throws Throwable {
		this.amount = arg1;
	}

	@When("^we get the employee's gross income per month$")
	public void we_get_the_employee_s_gross_income_per_month() throws Throwable {
		try {
			this.grossIncome = MonthlyAmountCalculator.findGrossIncome(this.amount);
		} catch (IllegalArgumentException e) {
			this.hasException = true;
		}
	}

	@Then("^the result should be (\\d+) for gross income testing$")
	public void the_result_should_be_for_gross_income_testing(int arg1) throws Throwable {
		Assert.assertEquals(arg1, grossIncome);
	}

	@Then("^the result should be FAILED for gross income testing$")
	public void the_result_should_be_FAILED_for_gross_income_testing() throws Throwable {
		Assert.assertTrue(hasException);
	}
}
