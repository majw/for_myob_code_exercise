package com.myob.strategy;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = { "src/test/resources/strategy/constructor.feature",
		"src/test/resources/strategy/findMonthlyIncomeTax.feature", "src/test/resources/strategy/withinRange.feature" } // refer
																														// to
// Feature
// file
)
public class TaxStrategyTest {
}
