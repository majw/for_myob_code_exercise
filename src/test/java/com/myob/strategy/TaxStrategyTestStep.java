package com.myob.strategy;

import org.junit.Assert;

import com.myob.payslip.generator.strategy.TaxStrategy;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TaxStrategyTestStep {

	TaxStrategy strategy;

	int base;

	double taxRate;

	int min;

	int max;

	int amount;

	int monthlyIncomeTax;

	boolean isInRange;

	boolean hasException;

	@Given("^the tax strategy configuration is (-?\\d+), (-?\\d*\\.?\\d+), (-?\\d+), (-?\\d+)$")
	public void the_tax_strategy_configuration_is(int arg1, double arg2, int arg3, int arg4) throws Throwable {
		this.base = arg1;
		this.taxRate = arg2;
		this.min = arg3;
		this.max = arg4;
	}

	@When("^we test the constructor$")
	public void we_test_the_constructor() throws Throwable {
		try {
			this.strategy = new TaxStrategy(base, taxRate, min, max);
		} catch (IllegalArgumentException exception) {
			this.hasException = true;
		}
	}

	@Then("^the execution of constructor should be true$")
	public void the_execution_of_constructor_should_be_true() throws Throwable {
		Assert.assertNotNull(this.strategy);
	}

	@Then("^the execution of constructor should be false$")
	public void the_execution_of_constructor_should_be_false() throws Throwable {
		Assert.assertTrue(hasException);
	}

	@Given("^the tax strategy configuration is (-?\\d+), (-?\\d*\\.?\\d+), (-?\\d+), (-?\\d+), and an amount (-?\\d+)$")
	public void the_tax_strategy_configuration_is_and_an_amount(int arg1, double arg2, int arg3, int arg4, int arg5)
			throws Throwable {
		this.strategy = new TaxStrategy(arg1, arg2, arg3, arg4);
		this.amount = arg5;
	}

	@When("^we test function findMonthlyIncomeTax$")
	public void we_test_function_findMonthlyIncomeTax() throws Throwable {
		try {
			this.monthlyIncomeTax = this.strategy.findMonthlyIncomeTax(this.amount);
		} catch (Exception exception) {
			this.hasException = true;
		}
	}

	@Then("^the return of findMonthlyIncomeTax should be (\\d+)$")
	public void the_return_of_findMonthlyIncomeTax_should_be(int arg1) throws Throwable {
		Assert.assertEquals(arg1, this.monthlyIncomeTax);
	}

	@Then("^the return of findMonthlyIncomeTax should be FAILED$")
	public void the_return_of_findMonthlyIncomeTax_should_be_FAILED() throws Throwable {
		Assert.assertTrue(hasException);
	}

	@When("^we test function withinRange$")
	public void we_test_function_withinRange() throws Throwable {
		try {
			this.isInRange = this.strategy.withinRange(this.amount);
		} catch (Exception exception) {
			this.hasException = true;
		}
	}

	@Then("^the return of withinRange should be false$")
	public void the_return_of_withinRange_should_be_false() throws Throwable {
		Assert.assertFalse(isInRange);
	}

	@Then("^the return of withinRange should be true$")
	public void the_return_of_withinRange_should_be_true() throws Throwable {
		Assert.assertTrue(isInRange);
	}

	@Then("^the return of withinRange should be FAILED$")
	public void the_return_of_withinRange_should_be_FAILED() throws Throwable {
		Assert.assertTrue(hasException);
	}
}
