package com.myob.generator;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = { "src/test/resources/generator/generate.feature" } // refer
// to
// Feature
// file
)
public class MonthlyPayslipGeneratorTest {
}
