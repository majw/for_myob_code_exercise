package com.myob.generator;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Assert;

import com.myob.payslip.generator.executor.MonthlyPayslipGenerator;
import com.myob.payslip.generator.model.IncomeInfo;
import com.myob.payslip.generator.model.Payslip;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MonthlyPayslipGeneratorTestStep {

	Collection<IncomeInfo> incomeInfos = new ArrayList<>();

	Collection<Payslip> payslips;

	@Given("^a income info model of Junwei MA$")
	public void a_income_info_model_of_Junwei_MA() throws Throwable {
		IncomeInfo junweima = new IncomeInfo();
		junweima.setFirstName("Junwei");
		junweima.setLastName("MA");
		junweima.setPaymentPeriod("Feb 01 - Feb 28");
		junweima.setSuperRate(0.091D);
		junweima.setAnnualSalary(140000);

		incomeInfos.add(junweima);
	}

	@Given("^a income info model of David Rudd$")
	public void a_income_info_model_of_David_Rudd() throws Throwable {
		IncomeInfo david = new IncomeInfo();
		david.setFirstName("David");
		david.setLastName("Rudd");
		david.setPaymentPeriod("01 March - 31 March");
		david.setSuperRate(0.09D);
		david.setAnnualSalary(60050);

		incomeInfos.add(david);
	}

	@Given("^a income info model of Ryan Chen$")
	public void a_income_info_model_of_Ryan_Chen() throws Throwable {
		IncomeInfo ryan = new IncomeInfo();
		ryan.setFirstName("Ryan");
		ryan.setLastName("Chen");
		ryan.setPaymentPeriod("01 March - 31 March");
		ryan.setSuperRate(0.1D);
		ryan.setAnnualSalary(120000);

		incomeInfos.add(ryan);
	}

	@When("^we test function generate$")
	public void we_test_function_generate() throws Throwable {
		this.payslips = MonthlyPayslipGenerator.generate(this.incomeInfos);
	}

	@Then("^the return of generate should match the expectation$")
	public void the_return_of_generate_should_match_the_expectation() throws Throwable {
		Assert.assertTrue(this.payslips.size() == 3);
		for (Payslip payslip : payslips) {
			switch (payslip.getName()) {
			case "Junwei MA":
				Assert.assertTrue(payslip.getPayPeriod().equalsIgnoreCase("Feb 01 - Feb 28"));
				Assert.assertTrue(payslip.getGrossIncome() == 11667);
				Assert.assertTrue(payslip.getIncomeTax() == 3312);
				Assert.assertTrue(payslip.getNetIncome() == 8355);
				Assert.assertTrue(payslip.getSuperAmount() == 1062);
				break;

			case "David Rudd":
				Assert.assertTrue(payslip.getPayPeriod().equalsIgnoreCase("01 March - 31 March"));
				Assert.assertTrue(payslip.getGrossIncome() == 5004);
				Assert.assertTrue(payslip.getIncomeTax() == 922);
				Assert.assertTrue(payslip.getNetIncome() == 4082);
				Assert.assertTrue(payslip.getSuperAmount() == 450);
				break;

			case "Ryan Chen":
				Assert.assertTrue(payslip.getPayPeriod().equalsIgnoreCase("01 March - 31 March"));
				Assert.assertTrue(payslip.getGrossIncome() == 10000);
				Assert.assertTrue(payslip.getIncomeTax() == 2696);
				Assert.assertTrue(payslip.getNetIncome() == 7304);
				Assert.assertTrue(payslip.getSuperAmount() == 1000);
				break;
			default:
				break;
			}
		}
	}
}
